import sharepy
import boto3
import os

def lambda_handler(event, context):
    # TODO implement
    #configure the AWS S3 bucket
    session = boto3.Session(
        aws_access_key_id = os.environ['aws_access_key_id'],
        aws_secret_access_key = os.environ['aws_secret_access_key'],
        region_name = os.environ['region_name'])
    s3 = session.resource('s3')
    
    
    # Authenticate the sharepoint
    s = sharepy.connect(os.environ['sharepoint_url'],username=os.environ['sharepoint_username'], password=os.environ['sharepoint_password'])
    
    #get the files from the URL
    file_structure = s.get(os.environ['sharepoint_url'] + "/sites/" + os.environ['sharepoint_sites'] + 
                    "/_api/web/GetFolderByServerRelativeUrl('/sites/" + os.environ['sharepoint_sites'] + "/" + os.environ['sharepoint_directory'] + "/')/Files")
    
    # Parsse the output
    file_lists=file_structure.json()['d']['results']
    #iterate over it and get the filename
    
    # Fetch the neccesary information
    for filename in file_lists:
        fname = filename['Name']
        print("file name from sharepoint: ", fname)
        urlLoad = f"{os.environ['sharepoint_url']}/sites/{os.environ['sharepoint_sites']}/_api/web/GetFileByServerRelativeUrl('/sites/{os.environ['sharepoint_sites']}/{os.environ['sharepoint_directory']}/{fname}')/$value"
        print(urlLoad)
        
        # Get the data from the file
        document = s.get(urlLoad)
        print("filename created in AWS: ", fname, document.status_code)
        
        # Push data in S3 guidion bucket
        bucket = s3.Object(os.environ['guidion_bucket'],fname)
        bucket.put(Body = document.content)


## Credentials set in Environment variables

# aws_access_key_id: AKIAIRJ2TFSLA6ANYQGA
# aws_secret_access_key: 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
# guidion_bucket: guidion-etl
# region_name: eu-central-1
# sharepoint_directory: Gui
# sharepoint_password: Celebal@123
# sharepoint_sites: Demo_
# sharepoint_url: https://celebaltech.sharepoint.com
# sharepoint_username: anil.agrawal@celebaltech.com
